/**
 * @file
 * Initialize history.back logic.
 */

(function ($, Drupal, once) {
  'use strict';

  Drupal.behaviors.previewerRefresh = {
    enabled: true,
    attach: (context) => {
      $(once('previewerRefresh', '.js-previewerRefresh', context)).each(() => {
        let $previewerRefreshButton = $(this);
        let $form = $previewerRefreshButton.closest('form');

        $form.on('formUpdated', (() => {
          if (!Drupal.behaviors.previewerRefresh.enabled) return;
          if ($('.js-offCanvasPreviewer').length === 0) return;

          Drupal.behaviors.previewerRefresh.enabled = false;
          $previewerRefreshButton.trigger('previewerRefreshSubmit');
          setTimeout(() => Drupal.behaviors.previewerRefresh.enabled = true, 1500);
        }));

        $previewerRefreshButton.on('previewerRefresh', (event, data) => {
          let $offCanvasPreviewer = $('.js-offCanvasPreviewerIframe');
          try {
            let $offCanvasPreviewerIframe = $offCanvasPreviewer[0];
            let scrollTop = $offCanvasPreviewerIframe.contentDocument.documentElement.scrollTop;
            $offCanvasPreviewerIframe.src += '';
            $offCanvasPreviewerIframe.onload = () => {
              $offCanvasPreviewerIframe.contentDocument.documentElement.scrollTop = scrollTop;
            }
          }
          catch(err) {
            $offCanvasPreviewer.replaceWith(data.content);
          }
        });
      });
    }
  };
}(jQuery, Drupal, once));
