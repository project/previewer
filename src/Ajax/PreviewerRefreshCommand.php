<?php

namespace Drupal\previewer\Ajax;

use Drupal\Core\Ajax\CommandInterface;
use Drupal\Core\Ajax\CommandWithAttachedAssetsTrait;

/**
 * AJAX command for invoking an arbitrary jQuery method.
 *
 * The 'invoke' command will instruct the client to invoke the given jQuery
 * method with the supplied arguments on the elements matched by the given
 * selector. Intended for simple jQuery commands, such as attr(), addClass(),
 * removeClass(), toggleClass(), etc.
 *
 * This command is implemented by Drupal.AjaxCommands.prototype.invoke()
 * defined in misc/ajax.js.
 *
 * @ingroup ajax
 */
class PreviewerRefreshCommand implements CommandInterface {

  use CommandWithAttachedAssetsTrait;

  /**
   * A CSS selector string.
   *
   * If the command is a response to a request from an #ajax form element then
   * this value can be NULL.
   *
   * @var string
   */
  protected string $selector;

  /**
   * A jQuery method to invoke.
   *
   * @var string
   */
  protected string $method;

  /**
   * An optional list of arguments to pass to the method.
   *
   * @var array
   */
  protected array $arguments;

  /**
   * The content for the matched element(s).
   *
   * Either a render array or an HTML string.
   *
   * @var string|array
   */
  protected mixed $content;

  /**
   * Constructs an InvokeCommand object.
   *
   * @param string $selector
   *   A jQuery selector.
   * @param mixed $content
   *   The content.
   */
  public function __construct(string $selector, mixed $content) {
    $this->selector = $selector;
    $this->content = $content;
    $this->method = 'trigger';
    $this->arguments = [
      'previewerRefresh',
      [
        'content' => $this->getRenderedContent()
      ]
    ];
  }

  /**
   * Implements Drupal\Core\Ajax\CommandInterface:render().
   */
  public function render(): array {

    return [
      'command' => 'invoke',
      'selector' => $this->selector,
      'method' => $this->method,
      'args' => $this->arguments,
    ];
  }

}
